# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#movies = Movie.create([{ title: 'Star Wars' }, { title: 'Lord of the Rings' }])
users = User.create([{name: 'Larissa Batista', email: 'larissa@gmail.com', password: '123456', password_confirmation: '123456'},
					 {name: 'William Ribeiro', email: 'william@gmail.com', password: '123456', password_confirmation: '123456'},
					 {name: 'Rafael Moreira', email: 'rafael@gmail.com', password: '123456', password_confirmation: '123456'}
					])
   
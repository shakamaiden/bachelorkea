class CreateUserMovies < ActiveRecord::Migration[5.1]
  def change
    create_table :user_movies do |t|
      t.references :user, foreign_key: true
      t.references :movie, foreign_key: true
      t.integer :rating
      t.text :comments
      t.boolean :watched

      t.timestamps
    end
    add_index :user_movies, [:user_id, :movie_id], unique: true 
  end
end

class CreateMovies < ActiveRecord::Migration[5.1]
  def change
    create_table :movies do |t|
      t.string :title, presence: true
      t.string :poster
      t.string :year, presence: true
      t.text :plot
      t.string :director, presence: true
      t.string :language, presence: true
      t.integer :metascore
      t.string :release_date
      t.string :imdbid

      t.timestamps
    end
    add_index :movies, :title
  end
end

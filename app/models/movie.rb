 # encoding: utf-8
class Movie < ApplicationRecord

  validates :title,:year, presence: true

  include HTTParty

  has_many :user_movies
  has_many :user, through: :user_movies

  def self.search(title)
    @moviesdb = where("title LIKE ?", "%#{title}%")
    @moviesapi =  Movie.apisearch(title)
    
    unless @moviesapi.empty?
    
      @moviesdb.each do |dbmovie|
        @moviesapi.delete_if { |movieapi| movieapi['imdbID'] == dbmovie.imdbid  }
      end
    end
    return @moviesdb, @moviesapi
  end

  

  def self.apisearch(title)
    response = HTTParty.get("http://www.omdbapi.com/?apikey=3cc176bb&s=#{title}").parsed_response
    if response['Response'] == "True"
      response['Search']
    else
       []
    end
  end

  def self.add_from_api(imdbID)
    response = HTTParty.get("http://www.omdbapi.com/?apikey=3cc176bb&i=#{imdbID}").parsed_response
    Movie.create(
      title: response['Title'],
      poster: response['Poster'],
      year: response['Year'],
      plot: response['Plot'],
      director: response['Director'],
      language: response['Language'],
      metascore: response['Metascore'],
      release_date: response['Released'],
      imdbid: response['imdbID']
    )
  end
end

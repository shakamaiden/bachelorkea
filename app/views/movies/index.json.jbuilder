json.array!(@movies) do |movie|
  json.extract! movie, :id, :title, :poster, :year, :plot, :director, :language, :metascore, :release_date
  json.url movie_url(movie, format: :json)
end

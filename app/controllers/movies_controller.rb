# encoding: utf-8
class MoviesController < ApplicationController
  before_action :set_movie, only: [:edit, :update, :destroy]

  # GET /movies
  # GET /movies.xml
  def index
    @movies = Movie.all
  end

  # GET /movies/new
  # GET /movies/new.xml
  def new
    @movie = Movie.new
  end

  # GET /movies/1/edit
  def edit
  end

  def show
    @movie = Movie.find(params[:id])
    unless @movie.blank?
      if user_signed_in?
        @addedmovie = UserMovie.find_by(user_id: current_user.id, movie_id: @movie.id)
      end
    end
  end

  # POST /movies
  # POST /movies.xml
  def create
    @movie = Movie.new(movie_params)
    respond_to do |format|
      if @movie.save
        format.html { redirect_to @movie, notice: 'movie was successfully created.' }
        format.json { render :show, status: :created, location: @movie }
      else
        format.html { render :new }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /movies/1
  # PUT /movies/1.xml
  def update
    respond_to do |format|
      if @movie.update(movie_params)
        format.html { redirect_to @movie, notice: 'Movie was successfully updated.' }
      else
        format.html { redirect_to edit_movie_path(@movie), notice: 'Movie was not updated.'}
      end
    end
  end

  # DELETE /movies/1
  # DELETE /movies/1.xml
  def destroy
    @movie.destroy
    respond_to do |format|
      format.html { redirect_to movies_url, notice: 'Movie was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def search
    @movies, @moviesAPI = Movie.search(params[:searchterm])
    respond_to do |format|
      format.html
      format.json
    end
  end

  def apicreate
    @movie = Movie.add_from_api(params[:imdbid])
    redirect_to @movie
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie
      @movie = Movie.find(params[:id])
    end
    def movie_params
      params.require(:movie).permit(:title, :poster, :year, :plot, :director, :language, :metascore, :release_date)
    end

end


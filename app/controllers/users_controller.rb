class UsersController < ApplicationController
  def index
   if user_signed_in?
    @friends = current_user.friends
   end
    if params[:searchterm]
      @users = User.search(params[:searchterm]).order("created_at DESC")
    end
  end

  def show
  	@user = User.find(params[:id])
    if user_signed_in?
      @friendship = Friendship.find_by(user_id: current_user.id, friend_id: @user.id)
      @watchedmovies = UserMovie.where(user_id: @user.id, watched: true)
      @todomovies = UserMovie.where(user_id: @user.id, watched: false)
    end
  end
end

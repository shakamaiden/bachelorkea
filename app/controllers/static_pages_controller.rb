class StaticPagesController < ApplicationController
  def home
    if user_signed_in?
      @todomoviescount = UserMovie.where(user_id: current_user.id, watched: false).count
      @watchedmovies = UserMovie.where(user_id: current_user.id, watched: true).count
    end
  end

  def help
  end
end

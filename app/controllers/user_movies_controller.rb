class UserMoviesController < ApplicationController
  def index
    if user_signed_in?
      @watchedmovies = UserMovie.where(user_id: current_user.id, watched: true)
      @todomovies = UserMovie.where(user_id: current_user.id, watched: false)
    end
	end

  def show
    @comments = UserMovie.where(movie_id: params['movie_id'])
  end

	def create
    if user_signed_in?
      @userMovie = UserMovie.new(user_id: current_user.id, movie_id: params['movie_id'], watched: false)
      respond_to do |format|
        if @userMovie.save
          puts "save "
          format.js {@success = true}
        else
            puts "don't save"
            format.js {@success = false}
        end
      end
    end
	end

	def update
		if user_signed_in?
			@usermovies = UserMovie.find_by(id: params[:id], user_id: current_user.id)
			respond_to do |format|
				if @usermovies.update(user_movies_params)
					format.js {@success = true}
				else
					format.js {@success = false}
				end
			end
		end
	end

  def destroy
    @usermovie = UserMovie.find_by(user_id: current_user.id, movie_id: params['movie_id'])
    @usermovie.destroy
    respond_to do |format|
      format.js { @movie_id = params[:movie_id] }
    end
  end

	private
		def user_movies_params
			params.require(:user_movie).permit(:user_id, :movie_id, :rating, :comments, :watched)
	end



end

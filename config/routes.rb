
Rails.application.routes.draw do

  get '/home',      to: 'static_pages#home'
  get '/help',      to: 'static_pages#help'
  get '/about',     to: 'static_pages#about'
  get '/contact',   to: 'static_pages#contact'
  get '/search',    to: 'movies#search'
  get '/apicreate', to: 'movies#apicreate'

  devise_for :users, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in' }

  devise_scope :user do
    get 'sign_in', to: 'devise/sessions#new'
    get 'sign_up', to: 'devise/registrations#new'
    get 'log_out', to: 'devise/sessions#destroy'
    get 'edit',    to: 'devise/registrations#edit'
  end

  post 'friendship', to: 'friendships#create'
  delete 'friendship', to: 'friendships#destroy'
  resources :users, only: [:index, :show]
  resources :movies
  resources :user_movies, except: [:destroy, :show, :edit]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'static_pages#home'
end
